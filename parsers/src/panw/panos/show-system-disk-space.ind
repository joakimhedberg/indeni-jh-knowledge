#! META
name: panos-show-system_disk-space 
description: fetch disk space utilization
type: monitoring
monitoring_interval: 10 minutes
requires:
    vendor: paloaltonetworks
    os.name: panos

#! COMMENTS
disk-usage-percentage:
    why: |
        Each device has multiple disk sections, or even separate disks. If any of those fill up, device stability may be impacted. Most mount points available in Palo Alto Networks firewalls have an auto-cleanup feature. Some do not, and require constant monitoring.
    how: |
        This alert uses the Palo Alto Networks API to retrieve the current utilization of the different disks and mount points (the equivelant of using the "show system disk-space" command).
    without-indeni: |
        An administrator will use the CLI to retrieve the storage utilization, normally once an issue occurs.
    can-with-snmp: true
    can-with-syslog: true
disk-used-kbytes:
    skip-documentation: true
disk-total-kbytes:
    skip-documentation: true

#! REMOTE::HTTP
url: /api?type=op&cmd=<show><system><disk-space></disk-space></system></show>&key=${api-key}
protocol: HTTPS

#! PARSER::AWK

function sizeToK(size) {
	if (size ~ /G/) {
    	sub(/G/, "", size)
    	size = size * 1024 * 1024
    } else if (size ~ /M/) {
    	sub(/M/, "", size)
    	size = size * 1024
    } else if (size ~ /K/) {
    	sub(/K/, "", size)
    	size = size
    }
    return size
}

BEGIN {
}

# Filesystem            Size  Used Avail Use% Mounted on
# /dev/sda3             3.8G  2.1G  1.6G  58% /
/(\d+)%/ {
    mount = $NF
    usage = $(NF-1)
    sub(/%/, "", usage)
    available = sizeToK($(NF-2))
    used = sizeToK($(NF-3))

    mounttags["file-system"] = mount
    writeDoubleMetricWithLiveConfig("disk-used-kbytes", mounttags, "gauge", "600", used, "Mount Points - Used Space", "kilobytes", "file-system")
    writeDoubleMetricWithLiveConfig("disk-total-kbytes", mounttags, "gauge", "600", used+available, "Mount Points - Total Space", "kilobytes", "file-system")
    writeDoubleMetricWithLiveConfig("disk-usage-percentage", mounttags, "gauge", "600", usage, "Mount Points", "percentage", "file-system")
}

