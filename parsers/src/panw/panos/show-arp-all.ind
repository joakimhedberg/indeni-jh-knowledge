#! META
name: panos-show-arp-all
description: fetch the arp data
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: paloaltonetworks
    os.name: panos
    product: firewall

#! COMMENTS
arp-total-entries:
    why: |
        A network device which forwards traffic needs to know the MAC addresses of devices it is directly connected to, so it can send traffic on layer 2. To do this, it uses ARP requests. The ARP replys are stored in a cache which allows the device to avoid doing ARP requests again and again for the same destination IP. The ARP cache has a finite size to avoid using up all of the available memory. If the ARP cache fills up with entries, some traffic may be dropped or drastically slowed down.
    how: |
        This alert uses the Palo Alto Networks API to retrieve the current utilization of the ARP cache - number of entries in it vs the total limit.
    without-indeni: |
        An administrator could write a script to leverage the Palo Alto Networks API to collect this data periodically and alert appropriately. Alternatively, wait for an issue to occur and check the ARP cache status by running "show arp all".
    can-with-snmp: false
    can-with-syslog: false
arp-table:
    why: |
        Tracking the ARP entry can indicate when certain hosts are failing to repsond to ARP requests. If that host is actually a next hop router, traffic may not reach its final destination. In addition, if there's a sudden jump in the number of ARP entries that are failing, it may indicate a connectivity issue at layer 2.
    how: |
        This alert uses the Palo Alto Networks API to retrieve the full ARP table for a Palo Alto Networks firewall, excluding the ARP table of the management interface (normally retrieved via \"show arp management").
    without-indeni: |
        An administrator could write a script to leverage the Palo Alto Networks API to collect this data periodically and alert appropriately. Alternatively, wait for an issue to occur and check the ARP cache status by running "show arp all".
    can-with-snmp: false
    can-with-syslog: false
arp-limit:
    skip-documentation: true

#! REMOTE::HTTP
url: /api?type=op&cmd=<show><arp><entry+name+%3D+%27all%27%2F><%2Farp><%2Fshow>&key=${api-key}
protocol: HTTPS

#! PARSER::XML
_vars:
    root: /response/result
_metrics:
    -
        _value.double:
            _text: ${root}/total
        _tags:
            "im.name":
                _constant: "arp-total-entries"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "ARP Cache - Current Entries"
            "im.dstype.displayType":
                _constant: "number"
    -
        _value.double:
            _text: ${root}/max
        _tags:
            "im.name":
                _constant: "arp-limit"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "ARP Cache - Limit"
            "im.dstype.displayType":
                _constant: "number"
    -
        _groups:
            "/response/result/entries/entry":
                _tags:
                    "im.name":
                        _constant: "arp-table"
                _temp:
                    "status":
                        _text: "status"
                _value.complex:
                    "targetip":
                        _text: ip
                    "mac":
                        _text: mac
                    "interface":
                        _text: "interface"
        _transform:
            _value.complex:
                "success": |
                    {
                        if (trim(temp("status")) == "i") { print "0"} else { print "1" }
                    }
        _value: complex-array
