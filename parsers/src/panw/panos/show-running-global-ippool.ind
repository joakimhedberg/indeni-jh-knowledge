#! META
name: panos-show-running-global-ippool
description: fetch NAT resource utilization
type: monitoring
monitoring_interval: 10 minutes
requires:
    vendor: paloaltonetworks
    os.name: panos
    product: firewall


#! COMMENTS
memory-usage:
    skip-documentation: true

#! REMOTE::HTTP
url: /api?type=op&cmd=<show><running><global-ippool></global-ippool></running></show>&key=${api-key}
protocol: HTTPS

#! PARSER::AWK
BEGIN {
}

# Used NAT DIP/DIPP shared memory size: 0(0.00%)
# Dynamic IP NAT Pool: 0(0.00%)
# Dynamic IP/Port NAT Pool: 0(0.00%)
/[0-9]+\.[0-9]+%/ {
    name=$0
    sub(/\:.*$/, "", name)
    memtags["name"] = name

    if (match($0, "\(([0-9]+\.[0-9]+)%\)")) {
        memory=substr($0, RSTART, RLENGTH - 1) # -1 is to exclude "%"
        writeDoubleMetricWithLiveConfig("memory-usage", memtags, "gauge", "600", memory, "Memory Usage", "percentage", "name")

    }
}
