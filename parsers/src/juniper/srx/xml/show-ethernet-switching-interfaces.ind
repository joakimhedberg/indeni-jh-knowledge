#! META
name: junos-show-ethernet-switching-interfaces
description: JUNOS SRX identifying which interface is disabled because of the vilation of the port-error-disable configuration
type: monitoring
monitoring_interval: 5 minute
requires:
     vendor: juniper
     os.name: junos
     product: firewall 

#! COMMENTS
network-interface-err-disable-status:
    why: |
        The port is disabled if port-error-disable is enabled and the port is configured to be shut down when it vilates the rules specified.     
    how: |
        The script runs "show ethernet-switching interfaces" command via ssh connection to the device and retrieves the port status related to port-error-disable rules configured.
    without-indeni: |
        An administrator could login to the device to manually run the command. 
    can-with-snmp: true  
    can-with-syslog: true 
    vendor-provided-management: |
        The same information can be retrieved via the command line.

#! REMOTE::SSH
show ethernet-switching interfaces | display xml

#! PARSER::XML
_vars:
    root: /rpc-reply//switching-interface-information[1]
_metrics:
    -
        _groups:
            ${root}/interface:
                _tags:
                    "im.name":
                        _constant: "network-interface-err-disable-status"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Network Interface Error Disable Status"
                    "im.dstype.displayType":
                        _constant: "state"
                    "im.identity-tags":
                        _constant: "name"
                    "im.identity-tags":
                        _constant: "blocking-status"
                    "blocking-status":
                        _text: interface-vlan-member-list/interface-vlan-member/blocking-status
                _temp:
                    status:
                        _text: interface-state
                    name:
                        _text: interface-name
        _transform:
            _tags:
                "name": |
                    {
                       print temp("name") 
                    }
            _value.double: |
                {
                    if (temp("status") == "up") {
                        print "1"
                    } else {
                        print "0"
                    }
                }
