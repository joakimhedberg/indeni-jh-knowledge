#! META
name: junos-show-route-summary
description: JUNOS retrieving the total of routes 
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: juniper
    os.name: junos

#! COMMENTS
routes-usage:
    why: |
        The number of routes might have negative impact on the device. 
    how: |
        This script retrieves the total of routes on the device by running "show route summary | match inet" command. 
    without-indeni: |
        An administrator could log on to the device to run the command "show route summary | match inet" to collect the same information.
    can-with-snmp: false 
    can-with-syslog: false
    vendor-provided-management: |
        The commamnd line is available to retrieve this information

#! REMOTE::SSH
show route summary | match inet 

#! PARSER::AWK
BEGIN{
  total_routes = 0
}

#inet.0: 15 destinations, 22 routes (15 active, 0 holddown, 0 hidden)
/inet/ {
   inet_table = $1
   gsub("\:", "", inet_table )
   name_tag["name"] = inet_table 
   total_for_table = $4
   writeDoubleMetric("routes-usage", name_tag, "gauge", 60, total_for_table)
   total_routes = total_routes + total_for_table 
}

END{
   name_tag["name"] = "ALL" 
   writeDoubleMetric("routes-usage", name_tag, "gauge", 60, total_routes)

}
