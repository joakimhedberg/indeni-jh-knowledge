#! META
name: junos-show-chassis-cluster-status
description: JUNOS collect clustering status
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: juniper
    os.name: junos
    high-availability: true

#! COMMENTS
cluster-member-active:
    why: |
        Tracking the state of a cluster member is important. If a cluster member which used to be the active member of the cluster no longer is, it may be the result of an issue. In some cases, it is due to maintenance work (and so was anticipated), but in others it may be due to a failure in the firewall or another component in the network.
    how: |
        This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show chassis cluster status" command. The output includes the status of all redundancy groups across the cluster.
    without-indeni: |
        The high availability status can be retrieved through SNMP and alerted through SNMP traps.
    can-with-snmp: true
    can-with-syslog: true
cluster-state:
    why: |
        Tracking the state of a cluster is important. If a cluster which used to be healthy no longer is, it may be the result of an issue. In some cases, it is due to maintenance work (and so was anticipated), but in others it may be due to a failure in the members of the cluster or another component in the network.
    how: |
        This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show chassis cluster status" command. The output includes the status of all redundancy groups across the cluster.
    without-indeni: |
        The high availability status can be retrieved through SNMP and alerted through SNMP traps.
    can-with-snmp: true
    can-with-syslog: true
cluster-preemption-enabled:
    why: |
        Preemption is a function in clustering which sets a primary member of the cluster to always strive to be the active member. The trouble with this is that if the active member that is set with preemption on has a critical failure and reboots, the cluster will fail over to the secondary and then immediately fail over back to the primary when it completes the reboot. This can result in another crash and the process would happen again and again in a loop.
    how: |
        This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show chassis cluster status" command. The output includes the status of all redundancy groups across the cluster.
    without-indeni: |
        The configuration of preemption is not available via SNMP and collection of this configuration will need to be done manually by an administrator.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
show chassis cluster status

#! PARSER::AWK

# IMPLEMENTATION NOTE:
# Why are we not using the XML version of this command? ("| display xml"). Because the XML isn't well formatted. Notice how two nodes are in a single XML section below:
#            <device-stats>
#                <device-name>node0</device-name>
#                <device-priority>1</device-priority>
#                <redundancy-group-status>primary</redundancy-group-status>
#                <preempt>no</preempt>
#                <failover-mode>no</failover-mode>
#                <monitor-failures>None</monitor-failures>
#                <device-name>node1</device-name>    --------- the same element name again!
#                <device-priority>0</device-priority>
#                <redundancy-group-status>lost</redundancy-group-status>
#                <preempt>n/a</preempt>
#                <failover-mode>n/a</failover-mode>
#                <monitor-failures>n/a</monitor-failures>
#            </device-stats>

#Node   Priority Status         Preempt Manual   Monitor-failures
/^Node.*Priority*/ {
    getColumns(trim($0), "[ \t]+", columns)
    globalpreempt = 0
}

#Redundancy group: 0 , Failover count: 1
/^Redundancy group/ {
    regroup = $3
    groupstatuses[regroup] = 1
}

#node0  1        primary        no      no       None           
/^node.*/ {
    node = $getColId(columns, "Node")
    statusDesc = $getColId(columns, "Status")
    preempt = $getColId(columns, "Preempt")

    if (statusDesc == "primary") {
        status = 1
    } else {
        status = 0
    }

    clustertags["name"] = "redundancy group " + regroup
    writeDoubleMetricWithLiveConfig("cluster-member-active", clustertags, "gauge", "60", groupstatuses[regroup], "Cluster Member Active", "state", "name")


    if (statusDesc != "primary" && statusDesc != "secondary") {
        # Bad status, write the cluster as down
        groupstatuses[regroup] = 0
    }

    if (preempt == "yes") {
        globalpreempt = 1
    }
}

END {
    for (regroup in groupstatuses) {
        clustertags["name"] = "redundancy group " + regroup
        writeDoubleMetricWithLiveConfig("cluster-state", clustertags, "gauge", "60", groupstatuses[regroup], "Cluster State", "state", "name")
    }

    writeDoubleMetricWithLiveConfig("cluster-preemption-enabled", null, "gauge", "60", globalpreempt, "Cluster Preemption Enabled", "boolean", "")

}
