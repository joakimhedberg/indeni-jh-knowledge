 #! META
name: f5-rest-mgmt-tm-ltm-node
description: Determine node state and availablity
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    rest-api: "true"
    shell: "bash"

#! COMMENTS
lb-server-state:
    why: |
        A node marked as down by a monitor, or disabled by an administrator, results in reduced pool capacity or in worst case, downtime. Disabling nodes is common during ie. a maintenance window but it is easily forgotten. This metric would warn administrators when a node is not ready to accept traffic.
    how: |
        This alert uses the iControl REST interface to extract the node statuses on the device.
    without-indeni: |
        Login to the device's web interface and click on "Local Traffic" -> "Nodes". This would show a list of the nodes and their statuses. In case the configuration is divided in multiple partitions changing to the "All [Read-only]" partition is recommended.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: Unknown
   
#! REMOTE::HTTP
url: /mgmt/tm/ltm/node?$select=fullPath,session,state
protocol: HTTPS

#! PARSER::JSON

_metrics:
    
    #   State determines if the node is ready to accept traffic according to the LB
    #
    #   Possible states: 
    #   checking - No result yet
    #   down - monitor down
    #   up - Monitor up
    #   unchecked - no assigned monitor
    #   user-down - Forced offline (disabled, and manually set to state down)
    #   unavailable - Ie. connection limit has been reached
    
    - # Record nodes ready to receive traffic
        _groups:
            "$.items[0:][?((@.state == 'up' || @.state == 'unchecked') && @.session != 'user-disabled')]":
                _tags:
                    "im.name":
                        _constant: "lb-server-state"
                    "name":
                        _value: "fullPath"
                _value.double:
                    _constant: "1"
    - # Record nodes not ready to receive traffic
        _groups:
            "$.items[0:][?((@.state != 'up' && @.state != 'unchecked') || @.session == 'user-disabled')]":
                _tags:
                    "im.name":
                        _constant: "lb-server-state"
                    "name":
                        _value: "fullPath"
                _value.double:
                    _constant: "0"
