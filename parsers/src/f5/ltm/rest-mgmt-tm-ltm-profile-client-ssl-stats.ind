 #! META
name: f5-rest-mgmt-tm-ltm-profile-client-ssl-stats
description: Determine ssl profile renegotiation statistics
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: "f5"
    product: "load-balancer"
    rest-api: "true"
    shell: "bash"
    
#! REMOTE::HTTP
url: /mgmt/tm/ltm/profile/client-ssl/stats?$select=tmName,common.secureHandshakes,common.insecureHandshakeAccepts,common.insecureHandshakeRejects,common.insecureRenegotiationRejects
protocol: HTTPS

#! PARSER::JSON

_metrics:
    - #Secure handshakes
        _groups:
            "$.entries.*.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "ssl-secure-handshakes"
                    "im.dsType":
                        _constant: "counter"
                    "im.dstype.displaytype":
                        _constant: "number"
                    "name":
                        _value: "tmName.description"
                _value.double:
                    _value: "['common.secureHandshakes'].value"
    - # Insecure handshakes accepted
        _groups:
            "$.entries.*.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "ssl-insecure-handshakes-accepted"
                    "im.dsType":
                        _constant: "counter"
                    "im.dstype.displaytype":
                        _constant: "number"
                    "name":
                        _value: "tmName.description"
                _value.double:
                    _value: "['common.insecureHandshakeAccepts'].value"
    - # Insecure handshakes rejected
        _groups:
            "$.entries.*.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "ssl-insecure-handshakes-rejected"
                    "im.dsType":
                        _constant: "counter"
                    "im.dstype.displaytype":
                        _constant: "number"
                    "name":
                        _value: "tmName.description"
                _value.double:
                    _value: "['common.insecureHandshakeRejects'].value" 
    - # Insecure renegotiations rejected
        _groups:
            "$.entries.*.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "ssl-insecure-renegotiations-rejected"
                    "im.dsType":
                        _constant: "counter"
                    "im.dstype.displaytype":
                        _constant: "number"
                    "name":
                        _value: "tmName.description"
                _value.double:
                    _value: "['common.insecureRenegotiationRejects'].value"
