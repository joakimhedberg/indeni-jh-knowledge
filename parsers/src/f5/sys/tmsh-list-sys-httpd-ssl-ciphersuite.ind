 #! META
name: f5-tmsh-list-sys-httpd-ssl-ciphersuite
description: Find usage of weak ciphers in the management interface
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    linux-based: "true"
    shell: "bash"

#! COMMENTS
ssl-weak-cipher:
    why: |
        Weak ciphers could allow for man in the middle attacks. Administrators would ideally want to keep track of their cipher string configurations in order to protect their clients against known attack vectors. This alert verifies that the management interface does not use any weak ciphers.
    how: |
        This alert logs into the F5 and retrieves the cipher strings being used by the management interface and scans for weak ciphers.
    without-indeni: |
        Log into the device through SSH. Enter TMSH and issue the command "cd /;list ltm profile client-ssl recursive ciphers renegotiation renegotiate-size" to retrieve a list of all SSL Client profiles and their ciphers. Then for each cipher string, issue the command "tmm --clientciphers <cipher string>". Example: "tmm --clientciphers '!LOW:!SSLv3:!MD5:!RC4:!DHE:!EXPORT:ECDHE+AES-GCM:ECDHE:RSA+AES:RSA+3DES'"
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: Unknown
ssl-weak-protocol:
    why: |
        Weak protocols could enable for man in the middle attacks. Administrators would ideally want to keep track of their cipher string configurations in order to protect their clients against known attack vectors. This alert verifies that the management interface does not use any weak protocols.
    how: |
        This alert logs into the F5 and retrieves the cipher strings being used by the management interface and scans for weak ciphers.
    without-indeni: |
        Log into the device through SSH. Enter TMSH and issue the command "cd /;list ltm profile client-ssl recursive ciphers renegotiation renegotiate-size" to retrieve a list of all SSL Client profiles and their ciphers. Then for each cipher string, issue the command "tmm --clientciphers <cipher string>". Example: "tmm --clientciphers '!LOW:!SSLv3:!MD5:!RC4:!DHE:!EXPORT:ECDHE+AES-GCM:ECDHE:RSA+AES:RSA+3DES'"
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: Unknown

#! REMOTE::SSH
tmsh -q -c "cd /;list sys httpd ssl-ciphersuite" | awk "/^ +ssl-ciphersuite/ {system(\"tmm --clientciphers \"\$2)}"
 
#! PARSER::AWK

#Cipher strings in an F5 can be comprised of a list of ciphers, or internal keywords such as DEFAULT, STRONG, WEAK etc
#Since the meaning of keywords changes between versions it's a bit of a trouble to write a script that matches all versions
#Instead, this script utilized F5's built-in command "tmm --clientciphers", which translates a cipher string to the actual
#ciphers behind the keywords

#12:    22  DHE-RSA-DES-CBC3-SHA             168  TLS1    Native  DES       SHA     EDH/RSA
/DES-CBC3/{
    #Ciphers vulnerable to Sweet32 detected
    sweet32Ciphers = 1
}

/SSL3/{
    #Make sure we're in the table by counting number of fields
    if($NF > 2){
        SSLv3 = 1
    }
}

/SSL2/{
    #Make sure we're in the table by counting number of fields
    if($NF > 2){
        SSLv2 = 1
    }
}

/RC4/{
    #Make sure we're in the table by counting number of fields
    if($NF > 2){
        RC4 = 1
    }
}

END {
        
    #Sweet32 uses long lived connections with enabled renegotiations
    #To mititgate Sweet32 renegotiations must be disabled, or either:
    #Rrenegotiation size set to less than 1000
    #The affected ciphers not present
    
    #Judging from F5's article about the matter renegotiation is always on on the management interface.
    #I have found no way of turning it off though, nor regulate the size

    if(sweet32Ciphers == 1){
        sweet32Tags["name"] = "httpd"
        sweet32Tags["type"] = "Management interface"
        sweet32Tags["vulnerability"] = "SWEET32"
        sweet32Tags["cipher"] = "DES-CBC3"
        writeDoubleMetric("ssl-weak-cipher", sweet32Tags, "gauge", 3600, 1)
    }
    
    #SSLv3 is consider not only weak, but more or less compromised (ie. Poodle)
    if(SSLv3 == 1){
        sslv3Tags["name"] = "httpd"
        sslv3Tags["type"] = "Management interface"
        sslv3Tags["protocol"] = "SSLv3"
        sslv3Tags["vulnerability"] = "POODLE"
        writeDoubleMetric("ssl-weak-protocol", sslv3Tags, "gauge", 3600, 1)
    }
    
    #SSLv2 is considered compromised (Drown)
    if(SSLv2 == 1){
        sslv2Tags["name"] = "httpd"
        sslv2Tags["type"] = "Management interface"
        sslv2Tags["protocol"] = "SSLv2"
        sslv2Tags["vulnerability"] = "DROWN"
        writeDoubleMetric("ssl-weak-protocol", sslv2Tags, "gauge", 3600, 1)
    }
    
    #RC4 is considered a weak cipher and there has been indications that is has
    #been compromised by certain government agencies
    if(RC4 == 1){
        rc4Tags["name"] = "httpd"
        rc4Tags["type"] = "Management interface"
        rc4Tags["cipher"] = "RC4"
        rc4Tags["vulnerability"] = "Bar Mitzvah"
        writeDoubleMetric("ssl-weak-cipher", rc4Tags, "gauge", 3600, 1)
    }
}