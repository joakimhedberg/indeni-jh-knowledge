#! META
name: f5-tmsh-list-sys-syslog
description: Check if a syslog server has been configured
type: monitoring
monitoring_interval: 10 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    linux-based: "true"
    shell: "bash"

#! COMMENTS
syslog-servers:
    why: |
        In case of an successful intrusion attempt it is imperative to be able to trust the log files. In order to be able to do that it is good to have a remote syslog server configured. That way the attacker would have a harder time to hide the tracks. Also, in case of an outage or hardware failure a remote syslog server could be critical in order to find the root cause.
    how: |
        This alert logs into the F5 device through SSH, parses the output of the command "tmsh list sys syslog" to verify that a syslog server has been configured
    without-indeni: |
        An administrator could could periodically log into the device through SSH, enter TMSH and execute the command "list sys syslog" in order to identify the configured syslog servers.
    can-with-snmp: false
    can-with-syslog: false
    
#! REMOTE::SSH

#!/bin/bash
tmsh -q -c "list sys syslog" | grep -P "(include|host)" | grep -Po "[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"

#! PARSER::AWK

BEGIN {
    iSyslog = 0
}

#192.168.1.3
/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/{
    iSyslog++
    syslogArray[iSyslog, "ip"] = $1
}

END {
    writeComplexMetricObjectArray("syslog-servers", null, syslogArray)
}
