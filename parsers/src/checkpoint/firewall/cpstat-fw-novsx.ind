#! META
name: cpstat_fw_novsx
description: run "cpstat fw" on non-vsx or VS0
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    vsx:
        neq: true
    role-firewall: "true"

#! COMMENTS
policy-installed:
    why: |
        If a security policy is not installed on the device, it will not be able to correctly forward traffic.
    how: |
        By using the Check Point built-in "cpstat fw" command, it is confirmed that a policy is installed.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing installed policy is possible through the Check Point SmartDashboard or the command line interface.

policy-install-last-modified:
    skip-documentation: true

#! REMOTE::SSH
${nice-path} -n 15 cpstat fw

#! PARSER::AWK

# Policy name: Standard
/Policy name/ {
    policyname=$0
    gsub(/Policy name\:/, "", policyname)
    policyname = trim(policyname)
}

# Install time: Mon May  9 03:05:41 2016
/Install time/ {
    installTime=$0
    # Remove the "install time:" part
	gsub(/Install time\:/, "", installTime)
	# Remove some double spaces
	gsub(/  /, " ", installTime)
    split(trim(installTime),installTimeArr," ")
	month_num=parseMonthThreeLetter(installTimeArr[2])
	day=installTimeArr[3]
	time=installTimeArr[4]
	split(time,timeArr,":")
	hour=timeArr[1]
	minute=timeArr[2]
	second=timeArr[3]
	year=installTimeArr[5]
}

END {
    if (policyname != "") {
        policystatus = 1
    } else {
        policystatus = 0
    }
	
	
	writeDoubleMetricWithLiveConfig("policy-installed", null, "gauge", "60", policystatus, "Firewall Policy", "state", "name")

	if (installTime != "") {
		writeDoubleMetricWithLiveConfig("policy-install-last-modified", null, "gauge", "60", datetime(year,month_num,day,hour,minute,second), "Firewall Policy - Last Modified", "date", "")
	}
}
