#! META
name: chkp-enabled_blades-novsx
description: show enabled features/blades
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: checkpoint
    role-firewall: true
    vsx: 
        neq: true
    or:
        -
            os.name: gaia
        -
            os.name: secureplatform

#! COMMENTS
features-enabled:
    why: |
        All members of a cluster should have the same features/blades enabled. By collecting this info it is possible to compare this between cluster members and alert if they do not have the same features enabled.
    how: |
        By using the Check Point built-in command "enabled_blades", a list of currently enabled software blades is retrieved. This command would need to be run on all members of a cluster.
    without-indeni: |
        An administrator could login and manually run the command or get the information from SmartDashboard.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        The best way is to retrieve this information is with Check Point SmartDashboard.

#! REMOTE::SSH
${nice-path} -n 15 enabled_blades ; ${nice-path} -n 15 enabled_blades.sh

#! PARSER::AWK

#########
# Script explanation: According to sk105573 the command can be either enabled_blades or enabled_blades.sh.
# We will run both command to make sure, but since both command could exist at the same time we only want to store the output of one of them
########

# nice: enabled_blades: No such file or directory
# fw vpn ips
/^[a-z]/ {
	if (metricWritten != 1 && $1 != "nice:") {
		split($0, featuresArr, " ")
		for (id in featuresArr) {
			ifeat++
			features[ifeat, "name"] = featuresArr[id]
		}
		writeComplexMetricObjectArray("features-enabled", null, features)
		metricWritten = 1
	}	
}
