#! META
name: fw_ctl_pstat_vsx
description: run "fw ctl pstat" on all vs's in VSX
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: checkpoint
    vsx: "true"
    role-firewall: true

#! COMMENTS
kernel-memory-usage:
    skip-documentation: true

chkp-agressive-aging:
    skip-documentation: true

concurrent-connections:
    skip-documentation: true

concurrent-connections:
    skip-documentation: true

virtual-memory-usage:
    skip-documentation: true


#! REMOTE::SSH
${nice-path} -n 15 fw vsx stat -l | grep VSID | awk '{print $NF}' | while read id; do vsenv $id && ${nice-path} -n 15 fw vsx stat $id && ${nice-path} -n 15 fw ctl pstat; done

#! PARSER::AWK

function addVsTags(tags) {
	tags["vs.id"] = vsid
    tags["vs.name"] = vsname
}

BEGIN {
	vsid=""
	vsname=""
}
/VSID:/ {
	vsid = trim($NF)
}
/Name:/ {
	vsname = trim($NF)
}

# Kernel   memory used:   9% (69 MB out of 703 MB) - below watermark
/Kernel\s+memory\s+used.*out of.*/ {
    usage=substr($4, 1, length($4)-1)
    addVsTags(vstags)
    writeDoubleMetricWithLiveConfig("kernel-memory-usage", vstags, "gauge", "60", usage, "Kernel Memory", "percentage", "vs.id|vs.name")
}

# Virtual   memory used:   9% (69 MB out of 703 MB) - below watermark
/Virtual\s+memory\s+used.*out of.*/ {
    usage=substr($4, 1, length($4)-1)
    addVsTags(vstags)
    writeDoubleMetricWithLiveConfig("virtual-memory-usage", vstags, "gauge", "60", usage, "Virtual Memory", "percentage", "vs.id|vs.name")
}

# Aggressive Aging is not active
/Aggressive Aging/ {
	message=trim($0)
	if (message == "Aggressive Aging is not active") {
		writeDoubleMetric("chkp-agressive-aging", vstags, "gauge", 60, 0)
	} else if (message == "Aggressive Aging is active") {
		writeDoubleMetric("chkp-agressive-aging", vstags, "gauge", 60, 1)
	}
}
