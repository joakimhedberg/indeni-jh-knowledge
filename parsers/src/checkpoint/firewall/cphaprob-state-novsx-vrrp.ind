#! META
name: cphaprob_state_monitoring-vrrp
description: run "cphaprob state" for cluster status monitoring for VRRP
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: checkpoint
    high-availability: "true"
    vrrp: "true"

#! COMMENTS
cluster-mode:
    skip-documentation: true

cluster-member-active:
    skip-documentation: true

cluster-member-states:
    skip-documentation: true

cluster-state:
    skip-documentation: true

#! REMOTE::SSH
stty rows 80 ; ${nice-path} -n 15 clish -c "show vrrp" && ${nice-path} -n 15 cphaprob state

#! PARSER::AWK

############
# Why: Important to know the status of the cluster
# How: Use both cphaprob state and show vrrp to get the complete picture

## This script is for VRRP clusters

# A remote peer can have the following states
# Down
# Active

# A local peer can have the following states
# Down
# Ready
# Active
# HA module not started.

### The cluster as a whole can have several states regarding different aspects of the cluster
## Redundancy
# The non-active member could be:
# Ready - This member will take over if the Active one goes down, but it is not syncing state tables, so the failover will cut all connections
# Down - This member will not take over in case the active member goes offline

## Health
# Active - This member is healthy and forwarding traffic

## VRRP
# A VRRP cluster shows both members as "Active" in cphaprob state. This is normal.
###########


BEGIN {
    foundActive="false"
    foundUnhealthyState="false"
    #allmembers="" # Will store the data regarding all cluster members
	t["name"] = "VRRP"
}

# Find out the real status on the local member, since cphaprob state always shows "Active" even when Standby
# In Backup state 2
# In Master state 0
/In Master state|In Backup state/{
	
	if ($2 == "Master") {
		nrMaster=$NF
	}
	if ($2 == "Backup") {
		nrBackup=$NF
	}
	if (nrMaster && nrBackup) {
		if (nrMaster > 0) {
			localRole="Active"
		} else {
			localRole="Standby"
		}
		
		if (nrBackup > 0) {
			remoteRole="Active"
		} else {
			remoteRole="Standby"
		}
	}
	
	
	
}

# Determine cluster mode
# Cluster Mode:   Sync only (OPSEC) with IGMP Membership
/Cluster Mode/ {
    clustermode="Sync only"
	
	writeComplexMetricString("cluster-mode", t, clustermode)
}

# Has the cluster at least one healthy member forwarding traffic?
# Match any cluster member being Active (but not Active Attention)
/^\d+\s.*Active$/ {
		foundActive="true"
}

# The state of the local cluster member
# 1 (local)  10.10.6.21      Active
/local/ {

	# Catch both "Active" and "Active Attention"
	if ($0 ~ /Active/) {
		if (localRole == "Active") {
			localState=1
		} else {
			localState=0
		}
	} else {
		localState=0
	}

	writeDoubleMetricWithLiveConfig("cluster-member-active", t, "gauge", "60", localState, "Cluster Member State (this)", "state", "cluster-member-state-description")
}


## Match any unhealthy statuses in the cluster

# Match any cluster member being down
# 1 (local)  10.11.2.21      0%              Down
/^\d+\s.*Down/ {
    foundUnhealthyState="true"
}

# Match any cluster member being in Ready state
# 1 (local)  10.11.2.21      0%              Ready
/^\d+\s.*Ready/ {
    foundUnhealthyState="true"
}

# Match local cluster member not having cluster services started
# HA module not started.
/HA module not started./ {
    foundUnhealthyState="true"
}


# Record all cluster members states into array
# Match any cluster state lines
# 1 (local)  10.10.6.21      Active
# 2          10.10.6.22      Down
/^\d+\s.*\d+\.\d+\.\d+\.\d+\s+/ {

	iallmembers++
	state=$NF
	assignedLoad=$(NF-1)
	uniqueIp=$(NF-1)

	
	allmembers[iallmembers, "state-description"] = state
	allmembers[iallmembers, "id"] = $1
	allmembers[iallmembers, "unique-ip"] = uniqueIp
	
	if ( $2 == "\(local\)") {
		allmembers[iallmembers, "is-local"] = 1
		# If state is "Active" then we need to determine the real status, Active or Standby
		if (state == "Active" && localRole) {	
			allmembers[iallmembers, "state-description"] = localRole
		}
	} else {
		allmembers[iallmembers, "is-local"] = 0	
		# If state is "Active" then we need to determine the real status, Active or Standby
		if (state == "Active" && remoteRole) {	
			allmembers[iallmembers, "state-description"] = remoteRole
		}
	}
}

END {

	# Not good if both members have active interfaces
	if (localRole == "Active" && remoteRole == "Active") {
		foundUnhealthyState="true"
	}
	
    # If we found at least one member forwarding traffic, and no cluster members in an unhealthy state then its ok
    clusterstate=0
    if (foundActive=="true" && foundUnhealthyState=="false") {
        clusterstate=1
    }
    writeDoubleMetricWithLiveConfig("cluster-state", t, "gauge", "60", clusterstate, "Cluster State", "state", "name")
   	
	# Write status of all members
	if (iallmembers != "") {
		writeComplexMetricObjectArray("cluster-member-states", t, allmembers)
   	}
}
