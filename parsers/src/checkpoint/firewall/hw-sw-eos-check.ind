#! META
name: hw-sw-eos-check
description: Checks when the software and hardware running has a end of support date
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: checkpoint

#! COMMENTS
software-eos-date:
    why: |
        When a system becomes end of life, support is no longer available. This is why planning to replace a system ahead of the end-of-support date is important.
    how: |
        By comparing the current software and hardware versions with the Check Point support documents, it is possible to alert ahead of time.
        https://www.checkpoint.com/support-services/support-life-cycle-policy/#softwaresupport
        https://www.checkpoint.com/support-services/support-life-cycle-policy/#appliancessupport
    without-indeni: |
        An administrator could login and manually run the command, then compare it with the information on Check Point's web page.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        The only method is to compare current hardware and software versons with the end of life dates for them, from Check Point's web page.

hardware-eos-date:
    skip-documentation: true

#! REMOTE::SSH
${nice-path} -n 15 cpstat os

#! PARSER::AWK

############
# Why: Compare todays date against the time when the current device will be out of support, to alert before that happens.
# How: Get EOS data from check point webpage, and compare with todays date.
# Caveats: EOS list needs to be updated manually.
###########

BEGIN {
	# Define all hardware and software end of support dates
	# Based on https://www.checkpoint.com/support-services/support-life-cycle-policy/#softwaresupport
	# Based on https://www.checkpoint.com/support-services/support-life-cycle-policy/#appliancessupport
	
	## Hardware
	
	# hardware[MODEL] = "DAY:MONTH:YEAR"
	
	# Model is only the model number, example: UTM-1 570 = 570
	# Month/day is in number with 2 digits
	# examples:
	#	hardware["570"] = "01:04:2017"
	
	# UTM-1
	hardware["130"] = "01:04:2017"
	hardware["270"] = "01:04:2017"
	hardware["570"] = "01:04:2017"
	hardware["1070"] = "01:04:2017"
	hardware["2070"] = "01:04:2017"
	hardware["3070"] = "01:04:2017"
	hardware["450"] = "01:10:2013"
	hardware["1050"] = "01:10:2013"
	hardware["2050"] = "01:10:2013"	
	
	
	# Power-1
	hardware["5070"] = "01:04:2017"	
	hardware["9070"] = "01:04:2017"
	hardware["11060"] = "01:04:2017"	
	hardware["11070"] = "01:04:2017"	
	hardware["11080"] = "01:04:2017"	
	
	
	# VSX
	hardware["11285"] = "01:04:2017"	
	hardware["11085"] = "01:04:2017"
	hardware["11275"] = "01:04:2017"
	hardware["11075"] = "01:04:2017"
	hardware["11265"] = "01:04:2017"
	hardware["11065"] = "01:04:2017"
	hardware["9090"] = "01:04:2017"
	hardware["9070"] = "01:04:2017"	
	hardware["3070"] = "01:04:2017"
	
	# Datacenter appliances
	hardware["21600"] = "31:10:2019"
	
	
	
	
	## Software

	# software[VERSION] = "MONTH:YEAR:HW"
	
	# Version is full version, example: R77.30
	# Month is in number with 2 digits
	# HW is only number model, example: 2200. Or generic if the HW doesnt matter
	# If more than one date/HW per version, separate with ","
	# the generic type needs to be at the end of the line
	# examples:
	# 	software["R70.20"] = "03:2013:generic"
	#	software["R75.20"] = "05:2017:600,05:2017:1100,08:2015:generic"
	
	# R70
	software["R70"] = "03:2013:generic"
	software["R70.1"] = "03:2013:generic"
	software["R70.20"] = "03:2013:generic"
	software["R70.30"] = "03:2013:generic"
	software["R70.40"] = "03:2013:generic"
	software["R70.50"] = "03:2013:generic"
	
	# R71
	software["R71"] = "04:2014:generic"
	software["R71.1"] = "04:2014:generic"
	software["R71.20"] = "04:2014:generic"
	software["R71.30"] = "04:2014:generic"
	software["R71.40"] = "04:2014:generic"	
	software["R71.45"] = "04:2014:generic"	
	software["R71.50"] = "04:2014:generic"	
	
	# R75
	software["R75"] = "04:2014:generic"	
	software["R75"] = "01:2015:generic"	
	software["R75"] = "01:2015:generic"
	software["R75.10"] = "01:2015:generic"	
	software["R75.20"] = "05:2017:600,05:2017:1100,08:2015:generic"
	software["R75.30"] = "08:2015:generic"
	software["R75.40VS"] = "07:2016:generic"
	software["R75.40"] = "04:2016:generic"
	software["R75.45"] = "04:2016:generic"
	software["R75.46"] = "04:2016:generic"
	software["R75.47"] = "04:2016:generic"
	
	# R76
	software["R76"] = "02:2017:generic"
	
	# R77
	software["R77"] = "08:2017:generic"
	software["R77.10"] = "08:2017:generic"
	software["R77.20"] = "05:2019:600,05:2019:700,05:2019:1100,05:2019:1200R,05:2019:1400,08:2017:generic"
	software["R77.30"] = "05:2019:generic"
}

# Product version Check Point Gaia R77.30
/SVN Foundation Version String:/ {
	version=$NF
}

# Model: Check Point 2200
/Appliance Name:/ {
	model=$NF
	# Remove any letters, for example the R in "1200R" appliance
	gsub(/[A-Z]/,"",model)
}


END {
	
	# Calculate and write software EOS date
	split(software[version],versionDataFullArr,",")

	for (i in versionDataFullArr) {
		split(versionDataFullArr[i],versionDataArr,":")
		
		# need logic to compare for ex model 1100 to 1122
		# To do this we will count the characters in the support string, and then strip the zeros
		# Then match the remaining and make sure its equal lenghts
		# Example:
		# 1100 appliance = 4 chars, 11
		# compare 11 to 1120 and make sure both are 4 chars lenght
		# This prevents matching against for example 11000 appliance
		modelCompare=versionDataArr[3]
		# Remove any letters, for example the R in "1200R" appliance
		gsub(/[A-Z]/,"",modelCompare)
		# Count chars of the model in support info
		modelCompareChars=length(modelCompare)
		# Count chars of the model on the device checked
		modelChars=length(model)

		# Remove zeros from the model name in support info
		gsub(/00/,"",modelCompare)

		if (  model ~ modelCompare &&  modelCompareChars == modelChars ) {
			if ( done != 1) {
				endSoftwareSupportMonth = versionDataArr[1]
				endSoftwareSupportYear = versionDataArr[2]
				writeDoubleMetricWithLiveConfig("software-eos-date", null, "gauge", 3600, date(endSoftwareSupportYear, endSoftwareSupportMonth, 1), "Software End of Support", "date", "")
				done=1
			}
		} else if ( versionDataArr[3] == "generic" ) {
			if ( done != 1) {
				endSoftwareSupportMonth = versionDataArr[1]
				endSoftwareSupportYear = versionDataArr[2]
				writeDoubleMetricWithLiveConfig("software-eos-date", null, "gauge", 3600, date(endSoftwareSupportYear, endSoftwareSupportMonth, 1), "Software End of Support", "date", "")
			}
		}
	}

	# Calculate and write HW EOS date
	split(hardware[model],modelDataArr,":")
	endHardwareSupportDay = modelDataArr[1]	
	endHardwareSupportMonth = modelDataArr[2]
	endHardwareSupportYear = modelDataArr[3]
	
	# Check that we actually got some data to write
	if ( endHardwareSupportDay ) {
		writeDoubleMetricWithLiveConfig("hardware-eos-date", null, "gauge", 3600, date(endHardwareSupportYear, endHardwareSupportMonth, endHardwareSupportDay), "Hardware End of Support", "date", "")
	}

}
