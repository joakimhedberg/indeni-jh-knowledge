#! META
name: chkp-gaia-clish_show_interfaces_all-vsx
description: run "show interfaces all" over clish in vsx
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: checkpoint
    os.name: gaia
    vsx: true
    role-firewall: true

#! COMMENTS
network-interface-state:
    skip-documentation: true

network-interface-admin-state:
    skip-documentation: true

network-interface-speed:
    skip-documentation: true

network-interface-duplex:
    skip-documentation: true

network-interface-ipv4-address:
    skip-documentation: true

network-interface-ipv4-subnet:
    skip-documentation: true

network-interface-type:
    skip-documentation: true

network-interface-mtu:
    skip-documentation: true

network-interface-mac:
    skip-documentation: true

network-interface-description:
    skip-documentation: true

network-interface-tx-bytes:
    skip-documentation: true

network-interface-rx-bytes:
    skip-documentation: true

network-interface-tx-packets:
    skip-documentation: true

network-interface-rx-packets:
    skip-documentation: true

network-interface-tx-errors:
    skip-documentation: true

network-interface-rx-dropped:
    skip-documentation: true

network-interface-tx-overruns:
    skip-documentation: true

network-interface-rx-overruns:
    skip-documentation: true

network-interface-tx-carrier:
    skip-documentation: true

network-interface-rx-frame:
    skip-documentation: true

network-interfaces:
    skip-documentation: true

#! REMOTE::SSH
${nice-path} -n 15 vsx stat -l | grep VSID | awk '{print $NF}' | while read id; do vsenv $id && ${nice-path} -n 15 vsx stat $id && (${nice-path} -n 15 ifconfig -a|grep "HWaddr"| awk {'print $1'}| while read interface; do ${nice-path} -n 15 ifconfig $interface && ${nice-path} -n 15 ethtool -i $interface && ${nice-path} -n 15 ethtool $interface ; ${nice-path} -n 15 grep "interface:$interface" /config/active; done); done

#! PARSER::AWK

function addVsTags(tags) {
	tags["vs.id"] = vsid
    tags["vs.name"] = vsname
}

function dumpVsInterfaceArrayData() {
	addVsTags(t)
	
	if (arraylen(interfaces)) {
		writeComplexMetricObjectArray("network-interfaces", t, interfaces)
	}
	
	# delete arrays between VSes
	delete interfaces
}

BEGIN {
	vsid = ""
	vsname = ""
}

# VSID:            0
/VSID:/ {
	if (vsid != "") {
		# write the previous VS's data
		dumpVsInterfaceArrayData()
	}
	vsid = trim($NF)
}

# Name:            VSX-CXL2-Gear
/Name:/ {
	vsname = trim($NF)
}

# LAN1.246      Link encap:Ethernet  HWaddr 00:1C:7F:23:26:EB
/Link encap:/ {
	addVsTags(stattags)
    interfaceName = trim($1)
    stattags["name"] = interfaceName
    interfaces[interfaceName, "name"] = interfaceName
	
	# Type
	type=$3
	gsub(/encap:/, "", type)
	writeComplexMetricString("network-interface-type", stattags, type)
	
	# MAC
	writeComplexMetricString("network-interface-mac", stattags, $5)
}


# inet addr:192.168.245.2  Bcast:192.168.245.255  Mask:255.255.255.0
/inet addr:/ {
	ip = $2
	subnet = $4
	
	gsub(/addr:/, "", ip)
	gsub(/Mask:/, "", subnet)
	
	writeComplexMetricString("network-interface-ipv4-address", stattags, ip)
	writeComplexMetricString("network-interface-ipv4-subnet", stattags, subnet)
	
	# make array for interfacename -> IP
	interfaceArr[interfaceName] = ip
}



# BROADCAST MULTICAST  MTU:1500  Metric:1
# UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
/MTU/ {
	# MTU
	mtu = $(NF-1)
	gsub(/MTU:/, "", mtu)
	interfaces[interfaceName, "mtu"] = mtu
	writeComplexMetricString("network-interface-mtu", stattags, mtu)
}

# RX bytes:3964467449 (3.6 GiB)  TX bytes:922468769 (879.7 MiB)
/X bytes/ {
    if ($1 == "TX") {
        txrxprefix = "tx"
    } else {
        txrxprefix = "rx"
    }

    for (i = 2; i<=NF; i++) {
        if (split($i, statparts, ":") == 2) {
            name = statparts[1]
            value = statparts[2]
            writeDoubleMetricWithLiveConfig("network-interface-" txrxprefix "-" name, stattags, "counter", "60", value, "Network Interfaces - " txrxprefix " " name, "number", "name")
        }
    }
}

# RX packets:33471990 errors:0 dropped:0 overruns:0 frame:0
/X packets/ {
    if ($1 == "TX") {
        txrxprefix = "tx"
    } else {
        txrxprefix = "rx"
    }

    for (i = 2; i<=NF; i++) {
        if (split($i, statparts, ":") == 2) {
            name = statparts[1]
            value = statparts[2]
            writeDoubleMetricWithLiveConfig("network-interface-" txrxprefix "-" name, stattags, "counter", "60", value, "Network Interfaces - " txrxprefix " " name, "number", "name")
        }
    }
}

# driver: vmxnet3
/driver:/ {
   interfaces[interfaceName, "driver"] = $2
}

# Speed: 100Mb/s
/Speed:/ {
	speed = $2
	gsub(/b\/s/, "", speed)
	writeComplexMetricString("network-interface-speed", stattags, speed)
}

# Duplex: Full
/Duplex:/ {
	if ($2 == "Full") {
		writeComplexMetricString("network-interface-duplex", stattags, "full")
	} else if ($2 == "Half") {
		writeComplexMetricString("network-interface-duplex", stattags, "half")
	}
}


# Link detected: yes
/Link detected:/ {
	if ($3 == "yes") {
		linkstate = 1
	} else {
		linkstate = 0
	}
	writeDoubleMetricWithLiveConfig("network-interface-state", stattags, "gauge", "60", linkstate, "Network Interfaces - Up/Down", "state", "name")
}

# Auto-negotiation: on
/Auto-negotiation:/ {
	interfaces[interfaceName, "auto-negotiation"] = $2
}

# interface:eth2:state on
/interface:[a-z]+[0-9]+:state/ {

	# Admin state
	if ($2 != "off") {
		adminstate=1
	} else {
		adminstate=0
	}
	
	# Write data
    writeDoubleMetricWithLiveConfig("network-interface-admin-state", stattags, "gauge", "60", adminstate, "Network Interfaces - Enabled/Disabed", "state", "name")
}

# interface:eth1:comments Private2\ lala
# interface:eth1:comments Test
/interface:[a-z]+[0-9]+:comments/ {
	# If the comment is in two words or more, a backslash is inserted. We need to remove it
	gsub(/interface:[a-z]+[0-9]+:comments/, "", $0)
	gsub(/(\\)/,"",$0)
	writeComplexMetricString("network-interface-description", stattags, trim($0))
}


END {
	writeComplexMetricObjectArray("network-interfaces", t, interfaces)
}