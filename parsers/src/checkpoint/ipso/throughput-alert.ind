#! META
name: chkp-ipso-throughput-alert
description: Check the current throughput for recieve and transmit for interfaces.
type: monitoring
monitoring_interval: 1 minutes
requires:
    vendor: checkpoint
    os.name: ipso

#! COMMENTS
network-interface-tx-util-percentage:
    why: |
        If the throughput reaches the limit, packets will be dropped.
    how: |
        Indeni will record the total bytes transmitted, wait a pre-determined amount of time and then record it again. By comparing before and after a value for how many bytes sent during the period of time can be determined.
    without-indeni: |
        An administrator could login and manually check this from the command line interface.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface or SmartView Monitor.

network-interface-rx-util-percentage:
    why: |
        If the throughput reaches the limit, packets will be dropped.
    how: |
        Indeni will record the total bytes received, wait a pre-determined amount of time and then record it again. By comparing before and after a value for how many bytes sent during the period of time can be determined.
    without-indeni: |
        An administrator could login and manually check this from the command line interface.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface or SmartView Monitor.

#! REMOTE::SSH
${nice-path} -n 15 ifconfig -a ; netstat -idb ; sleep 10 ; netstat -idb 

#! PARSER::AWK

##################################
#
#	Determine interface name
#	
##################################

# eth1c0:  lname eth1c0 flags=e0<BROADCAST,MULTICAST,AUTOLINK>
/^[a-z0-9]+: / {
    interfaceName = $1
    sub(/:/, "", interfaceName)

    # netstat uses interface names with the "c0" (the second identifier), so we need to drop it
    if (interfaceName ~ /[A-Za-z]+[0-9]+[A-Za-z]+[0-9]+/) {
    	sub(/[A-Za-z]+[0-9]+$/, "", interfaceName)
    }
}

#eth1         16018 <Link>      0:a0:8e:b2:22:34        0     0          0        0     0          0     0   0
/^[a-z0-9]+ .*[0-9]+.*:/ {
	interfaceName = $1
}

##################################
#
#	Record bytes sent/received
#	
##################################

# Name         Mtu   Network     Address             Ipkts Ierrs     Ibytes    Opkts Oerrs     Obytes  Coll Drop
# eth2         16018 <Link>      0:a0:8e:b2:22:35    39706   440    5481041    28917     0   10158270  1898   0
/Name.*Mtu.*Network/ {
    inNetstat = "true"
    getColumns(trim($0), "[ \t]+", columns)
}

# eth2         16018 <Link>      0:a0:8e:b2:22:35    39706   440    5481041    28917     0   10158270  1898   0
/^[a-z0-9]+ .*[0-9]+.*:/ {
	bytesRx = getColData(trim($0), columns, "Ibytes")
	bytesTx = getColData(trim($0), columns, "Obytes")
	
	if (interfaceName in bytesTxArr) { # if the array bytesTxArr already contains data for the interface
		# Calculate diff
		# How many bytes sent first run - how many bytes sent second run
		# Divided by 10 - To get into per second as first and second run was 10 seconds apart
		# Times 8 - convert from byte to bit
		# Divided into 1000 - Get into kilobit
		bytesTxArr[interfaceName] = (((bytesTx - bytesTxArr[interfaceName]) / 10) * 8) / 1000  # Result kilobit per second
	} else {
		bytesTxArr[interfaceName] = bytesTx
	}
	
	if (interfaceName in bytesRxArr) { # if the array bytesRxArr already contains data for the interface
		# Calculate diff
		# How many bytes sent first run - how many bytes sent second run
		# Divided by 10 - To get into per second as first and second run was 10 seconds apart
		# Times 8 - convert from byte to bit
		# Divided into 1000 - Get into kilobit
		bytesRxArr[interfaceName] = (((bytesRx - bytesRxArr[interfaceName]) / 10) * 8) / 1000 # Result kilobit per second
	} else {
		bytesRxArr[interfaceName] = bytesRx
	}
}

##################################
#
#	Determine speed of interface
#	
##################################

#         ether 00:a0:8e:b2:22:37 speed 10M half duplex
/ speed .* duplex/ {
    speed = $0
    sub(/.* speed /, "", speed)
    sub(/ .*/, "", speed)

    if (speed ~ /M/) {
    	speedUnit = "M"
    }

    speedData = speed
    sub(/[A-Z]/, "", speedData)

	# Have not seen prefix on interface speed be anything else than Mega so far, but this would make it easy to add others if needed in the future.
	if (speedUnit == "M") {
		speedKbitArr[interfaceName] = speedData * 1000
	}
}

END {

	##################################
	#
	#	Remove interfaces that do not 
	#	have a known speed
	#	
	##################################
	for (interface in bytesTxArr) {
		if (!(interface in speedKbitArr)) {
			delete bytesTxArr[interface]
		}	
	}
	
	for (interface in bytesRxArr) {
		if (!(interface in speedKbitArr)) {
			delete bytesRxArr[interface]
		}	
	}
	
	##################################
	#
	#	Calculate percentage interface 
	#	usage and write metric data
	#	
	##################################	

	for (interface in bytesTxArr) {
		interfaceTags["name"] = interface
		percentageTxUsed[interface] = (bytesTxArr[interface] / speedKbitArr[interface]) * 100
		writeDoubleMetricWithLiveConfig("network-interface-tx-util-percentage", interfaceTags, "gauge", "60", percentageTxUsed[interface], "Network Interfaces - Throughput Transmit", "percentage", "name")
	}
	
	for (interface in bytesRxArr) {
		interfaceTags["name"] = interface
		percentageRxUsed[interface] = (bytesRxArr[interface] / speedKbitArr[interface]) * 100
		writeDoubleMetricWithLiveConfig("network-interface-rx-util-percentage", interfaceTags, "gauge", "60", percentageRxUsed[interface], "Network Interfaces - Throughput Receive", "percentage", "name")
	}
}