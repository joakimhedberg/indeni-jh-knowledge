#! META
name: chkp-clish-show-bgp
description: Check status of BGP peers
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: checkpoint
    routing-bgp: true
    or:
        -
            os.name: gaia
        -
            os.name: ipso

#! COMMENTS
bgp-state:
    why: |
        If BGP peers have connection issues it could mean whole sites going offline. Detecting it early is critical.
    how: |
        The clish command "show bgp peers" is used to retrieve the current BGP peer states.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing the clish BGP peer state is only available from the command line interface.

#! REMOTE::SSH
stty rows 80 ; ${nice-path} -n 15 clish -c 'show bgp peers'

#! PARSER::AWK

# The script only accept states "Established" and "Ide" as valid states as they are the only states that would be acceptable during a longer period of time. Some other states will be present during the setup phase of a BGP connection, but they should not remain. Also technically they mean that no data is flowing yet.
# BGP states: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk40560

#215.117.172.229  3356   1       1       Established       3       1        5w5d
#215.117.172.230  3356   0       0       Idle              0       0        00:00:00
/^[0-9]/ {
	stateMessage = $5
	name = $1
	
	if (stateMessage == "Established" || stateMessage == "Idle") {
		status = 1
	} else {
		status = 0
	}
	
	tags["name"] = name
	writeDoubleMetric("bgp-state", tags, "gauge", 300, status)
}
