#! META
name: ios-show-running-config
description: IOS show running-config (with include filters)
type: monitoring
monitoring_interval: 60 minute
requires:
    vendor: cisco
    os.name: ios

#! COMMENTS
domain:
    why: |
      Check that the right domain name is configured on the device.
    how: |
       This script logs into the Cisco IOS device using SSH and uses the "show running-config" command to find the configured domain name.
    without-indeni: |
       It is not possible to poll this data through SNMP or Syslog.
    can-with-snmp: false
    can-with-syslog: false

login-banner:
    why: |
      Check that the right login banner is configure on the device.
    how: |
       This script logs into the Cisco IOS device using SSH and uses the "show running-config" command to find the configured login banner.
    without-indeni: |
       It is not possible to poll this data through SNMP or Syslog.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
# show running-config | include ip domain name
# show running-config | begin banner login
show running-config


#! PARSER::AWK

BEGIN {
    entry=0
    domain_name="Not Defined"
}

/^ip domain name/ {
    
    domain_name = trim($4)
    writeDebug("Found domain: " domain_name)
    writeDebug("")
    
    #sub(/^C/, "", domain_name)
}

# SAMPLE BANNER
# banner login ^C
# Unauthorized Login
# This Device is managed by the ACME corporation
# If you have not been explicitly granted access to this device logoff immediately
# LAST WARNING
# ^C
/^banner login/{

    # Determine the banner type (login, exec, motd)
    banner_type = ""
    banner_text = ""
   
    # Find out character used for delimeter - last field entry 
    delimeter = trim($NF)

    # populate banner type
    for (i=1; i <= NF-1; i++) { banner_type = trim(banner_type " " $i) }
    sub(/banner /, "", banner_type)
    writeDebug("")
    writeDebug("1. Type ")
    writeDebug(banner_type)

    # move to next line, where banner actually starts
    getline
    writeDebug("2. " $0)
    # and add line to banner_text
    banner_text = banner_text " \n\n" trim($0)
   
    # iterate next lines until 'delimeter' is encountered 
    while ( trim($NF) != trim(delimeter) ) {
        iter++
        getline
        if ($0 == delimeter) { break }
        writeDebug("3. " $0)
        banner_text = banner_text " \\n" trim($0) 
    }
    writeDebug("4. This is the entire banner ... " iter)
    writeDebug("   " banner_type " \\n" banner_text )
   
    # for each banner (i.e. login, motd, etc.) create entry 
    entry++
    # bannertag[entry, "banner-type"] = trim(banner_type)
    bannertag[entry, "value"] = trim(banner_text)  
    
}


    

END {

    domain_tags["display-name"] = "Domain Name"
    writeComplexMetricString("domain", domain_tags, domain_name)

    # writeComplexMetricObjectArray("login-banner", null, bannertag)
    writeComplexMetricString("login-banner", null, banner_text)
}
