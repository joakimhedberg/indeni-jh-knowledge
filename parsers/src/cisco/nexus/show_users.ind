#! META
name: nexus-license-usage
description: Nexus license information
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: cisco
    os.name: nxos

#! COMMENTS
logged-in-users:
    why: |
       Check for any logged in users and identify idle/stale terminal sessions. Too many idle sessions can block additional user logins to the device.
    how: |
       This script logs into the Cisco Nexus switch using SSH and uses the "show users" command to check if there are any idle terminal sessions. It would report the username, source IP/host and the idle time.
    without-indeni: |
       It is not possible to poll this data through SNMP. Configuration changes are reported in Syslog, but the saved/unsaved state is not reported.
    can-with-snmp: false
    can-with-syslog: false


#! REMOTE::SSH
show users | xml

#! PARSER::XML
_omit:
    _bottom: 1
_vars:
    root: //__readonly__
_metrics:
    -
        _groups:
            ${root}/TABLE_sessions/ROW_sessions:
                _temp:
                    idle:
                        _text: "t_idle"
                    comment:
                        _text: "c_comment"
                    terminal:
                        _text: "t_terminal"
                _value.complex:
                    user:
                        _text: "u_name"
                _tags:
                    "im.name":
                        _constant: "logged-in-users"
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "Logged In Users"
        _transform:
            _value.complex:
                idle: |
                    {
                        idle = trim(temp("idle"))
                        if (idle == ".") {
                            print "0"
                        } else {
                            split(idle, timearr, ":")
                            print timearr[1]*60 + timearr[2]
                        }
                    }
                from: |
                    {
                        comment = temp("comment")
                        gsub(/[*]/, "", comment)
                        comment = trim(comment)
                        print temp("terminal") " " comment
                    }
                
        _value: complex-array
