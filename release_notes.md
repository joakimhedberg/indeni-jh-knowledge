##Version 6.0.0
###Feature - IS-2723:: Improve AWK parser 
The AWK parser consumes *a lot* of CPU time -- way more than it should. We should improve its performance. 
According to the documentation in the [JAWK website](http://jawk.sourceforge.net/), there are two potentials ways in which we could improve the parser's performance significantly:
* Compile the AWK scripts on start-up instead of using an interpreter each time.
* Add a custom extension for the built-in AWK functions (in {{indeni.hawk}}) that'll remove our need to parse the AWK script's output with regex-es (which is also a very heavy operation).