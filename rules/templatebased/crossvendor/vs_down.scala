package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}

/**
  *
  */
case class vs_down(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "vs_down",
  ruleFriendlyName = "Devices with VS's: VS(s) down",
  ruleDescription = "indeni will alert one or more virtual systems in a device is down.",
  metricName = "vs-state",
  applicableMetricTag = "name",
  alertItemsHeader = "VS's Affected",
  alertDescription = "One or more virtual systems in this device are down.",
  baseRemediationText = "Review the cause for the virtual systems being down.")(
  ConditionalRemediationSteps.VENDOR_F5 -> "For F5 vCMP's, review https://support.f5.com/csp/article/K15930"
)
