package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, NearingCapacityTemplateRule}

/**
  *
  */
case class routes_defined_limit_ipv6(context: RuleContext) extends NearingCapacityTemplateRule(context,
  ruleName = "routes_defined_limit_ipv6",
  ruleFriendlyName = "All Devices: Maximum number of routes nearing (IPv6)",
  ruleDescription = "Many devices have a limit for the number of IPv6 routes that can be defined. Indeni will alert prior to the number of routes reaching the limit.",
  usageMetricName = "routes-usage-ipv6",
  limitMetricName = "routes-limit-ipv6",
  threshold = 80.0,
  alertDescriptionFormat = "There are %.0f IPv6 routes defined where the limit is %.0f.",
  baseRemediationText = "Consider removing certain routes.")(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |1. Execute the "show ip route summary" command to display the current contents of the IPv6 routing table in summary format.
      |2. Consider to deploy route summarization to decrease the  total number of ipv6 prefixes.
      |3. Consider to clean up the configuration from unused routes.
    """.stripMargin
)
