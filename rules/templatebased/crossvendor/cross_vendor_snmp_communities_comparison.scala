package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, SnapshotComparisonTemplateRule}

/**
  *
  */
case class cross_vendor_snmp_communities_comparison(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "cross_vendor_snmp_communities_comparison",
  ruleFriendlyName = "Clustered Devices: SNMP community settings do not match across cluster members",
  ruleDescription = "Indeni will identify when two devices are part of a cluster and alert if the SNMP settings do not match.",
  metricName = "snmp-communities",
  isArray = true,
  alertDescription = "Devices that are part of a cluster should have the same SNMP configuration. Review the differences below.",
  baseRemediationText = "Ensure all of the SNMP settings are configured correctly on all cluster members.")(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |1. Ensure all of the SNMPv2 communities are configured correctly on all cluster members by using  the "show snmp" NX-OS command.
      |2. Configure the same SNMPv2c communities by using the next command "snmp-server community name group { ro | rw }" across the peer Switches.
      |3. For more information please review  the next CISCO configuration guide:
      |https://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/5_x/nx-os/system_management/configuration/guide/sm_nx_os_cg/sm_9snmp.html
    """.stripMargin
)
