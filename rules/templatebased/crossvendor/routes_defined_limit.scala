package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, NearingCapacityTemplateRule}

/**
  *
  */
case class routes_defined_limit(context: RuleContext) extends NearingCapacityTemplateRule(context,
  ruleName = "routes_defined_limit",
  ruleFriendlyName = "All Devices: Maximum number of routes nearing (IPv4)",
  ruleDescription = "Many devices have a limit for the number of IPv4 routes that can be defined. Indeni will alert prior to the number of routes reaching the limit.",
  usageMetricName = "routes-usage",
  limitMetricName = "routes-limit",
  threshold = 80.0,
  alertDescriptionFormat = "There are %.0f IPv4 routes defined where the limit is %.0f.",
  baseRemediationText = "Consider removing certain routes.")(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |1. Execute the "show ip route summary" command to display the current contents of the IPv4 routing table in summary format.
      |2. Consider to deploy route summarization to decrease the  total number of ipv4 prefixes.
      |3. Consider to clean up the configuration from unused routes.
    """.stripMargin
)
