package com.indeni.server.rules.library.templatebased.checkpoint

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.SnapshotComparisonTemplateRule

/**
  *
  */
case class checkpoint_compare_jumbo_hotfix(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "checkpoint_compare_jumbo_hotfix",
  ruleFriendlyName = "Check Point Cluster: Jumbo Hotfix Take mismatch across cluster members",
  ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the jumbo hot fix installed is different.",
  metricName = "hotfix-jumbo-take",
  isArray = false,
  alertDescription = "The members of a cluster of Check Point firewalls must have the same jumbo hot fix installed.",
  baseRemediationText = """Compare the output of "show installer package" (under CLISH) across members of the cluster.""")()
