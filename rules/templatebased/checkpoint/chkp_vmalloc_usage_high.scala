package com.indeni.server.rules.library.templatebased.checkpoint

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.NearingCapacityTemplateRule

/**
  *
  */
case class chkp_vmalloc_usage_high(context: RuleContext) extends NearingCapacityTemplateRule(context,
  ruleName = "chkp_vmalloc_usage_high",
  ruleFriendlyName = "Check Point Firewalls: High vmalloc usage",
  ruleDescription = "indeni will alert when usage of vmalloc is high.",
  usageMetricName = "vmalloc-used-kbytes",
  limitMetricName = "vmalloc-total-kbytes",
  threshold = 80.0,
  alertDescriptionFormat = "vmalloc is using %.0f kilobytes where the limit is %.0f.",
  baseRemediationText = "Review sk84700.")()
