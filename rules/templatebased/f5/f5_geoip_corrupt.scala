package com.indeni.server.rules.library.templatebased.f5

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

/**
  *
  */
case class f5_geoip_corrupt(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "f5_geoip_corrupt",
  ruleFriendlyName = "F5 Devices: Non-functioning geo-ip database",
  ruleDescription = "indeni will alert if the geo-ip database is corrupted.",
  metricName = "geoip-database-state",
  applicableMetricTag = "database",
  alertItemsHeader = "Affected Databased",
  alertDescription = "The geo-ip database provides meta data related to an IP address, such as city, region, country and ISP. Should the database be unavailable any attempts to retrieve geo-ip data could cause unpredictable behavior.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"https://se.linkedin.com/in/patrik-jonsson-6527932\">Patrik Jonsson</a>.",
  baseRemediationText = "Contact F5 support.")()
