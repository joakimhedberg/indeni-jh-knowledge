package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.{And, Equals}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.rules._
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.server.rules.library.{ConditionalRemediationSteps, RuleHelper}


case class PortIsDownRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder("cross_vendor_network_port_down", "All Devices: Network port(s) down",
    "Indeni will alert one or more network ports is down.", AlertSeverity.ERROR).build()

  override def expressionTree: StatusTreeExpression = {

    val actualValue = TimeSeriesExpression[Double]("network-interface-state").last
    val adminValue = TimeSeriesExpression[Double]("network-interface-admin-state").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(

        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("network-interface-state", "network-interface-admin-state")),

        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("network-interface-state", "network-interface-admin-state"), denseOnly = false),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          And(
            Equals(ConstantExpression[Option[Double]](Some(0)), actualValue),
            Equals(ConstantExpression[Option[Double]](Some(1)), adminValue)
          )

          // The Alert Item to add for this specific item
        ).withSecondaryInfo(
          scopableStringFormatExpression("${scope(\"name\")}"),
          EMPTY_STRING,
          title = "Ports Affected"
        ).asCondition()
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("One or more ports are down."),
      ConditionalRemediationSteps("Review the cause for the ports being down.",
        ConditionalRemediationSteps.OS_NXOS ->
          """1. Check the physical media to ensure that there are no damaged parts.
            |2. Verify that the SFP (small form-factor pluggable) devices in use are those authorized by Cisco and that they are not faulty by executing the “show interface transceiver” NX-OS command.
            |3. Verify that you have enabled the port by using the "no shutdown" NX-OS command.
            |4. Use the "show interface" command to verify the state of the interface. Besides, you can use the “show interface counters” command to check port counters.
            |5. Check if the port is configured in dedicated mode.
            |6. Execute the following NX-OS commands to gather more information about ports:
            |• "show interface status"
            |• "show interface capabilities"
            |• "show udld"
            |• "show tech-support udld"
            |
            |7. For more information review: <a target="_blank" href="https://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus9000/sw/7-x/troubleshooting/guide/b_Cisco_Nexus_9000_Series_NX-OS_Troubleshooting_Guide_7x/b_Cisco_Nexus_9000_Series_NX-OS_Troubleshooting_Guide_7x_chapter_0101.pdf">Nexus Troubleshooting Guide</a> """.stripMargin
      )
    )
  }
}



